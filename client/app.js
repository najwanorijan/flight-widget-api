//to bring the dta from api and bring to frontend

const tableBody = document.getElementById("table-body");

const getFlight = () => {
  fetch("http://localhost:8000/flights")
    .then((response) => response.json())
    .then((flights) => {
      populateTable(flights); //pass data to populateTable
    })
    .catch((err) => console.log(err));
};

getFlight(); //wen want to test, uncomment this

const populateTable = (flights) => {
  console.log(flights);
  // we do like this bcs each row takes table datas
  for (const flight of flights) {
    const tableRow = document.createElement("tr");
    const tableIcon = document.createElement("td");
    tableIcon.textContent = "✈";
    tableRow.append(tableIcon);

    console.log("flight.flightNumber", flight.flightNumber);

    // redefine our object, for the one in index.html
    const flightDetails = {
      time: flight.departing,
      destination: flight.destination.toUpperCase(),
      flight: flight.flightNumber.shift(), //add shift method to return only the first value, as currently it will return in array ex: [ 'AC8928' ]
      gate: flight.gate,
      remarks: flight.status.toUpperCase(),
    };

    for (const flightDetail in flightDetails) {
      const tableCell = document.createElement("td");
      const word = Array.from(flightDetails[flightDetail]);

      for (const [index, letter] of word.entries()) {
        const letterElement = document.createElement("div");

        //100 millisecond, we make times index to make the first one flip over 0ms and second will flip on 100 millisecnd and third will flip over 300 millisecond and so on
        setTimeout(() => {
          // how we add class using javascript
          letterElement.classList.add("flip");
          letterElement.textContent = letter;
          tableCell.append(letterElement);
        }, 100 * index);
      }
      tableRow.append(tableCell);
    }
    tableBody.append(tableRow);
  }
};
