const PORT = 8000;
const axios = require("axios").default;
const express = require("express");
const app = express();

const cors = require("cors"); //if got error block by cors means need to check other tutorial about cors
require("dotenv").config(); // to hide our rapid api key
app.use(cors());

app.get("/flights", (req, res) => {
  const axios = require("axios");

  const options = {
    method: "GET",
    url: "https://toronto-pearson-airport.p.rapidapi.com/departures",
    headers: {
      "X-RapidAPI-Key": process.env.RAPID_API_KEY, //take the key from here
      "X-RapidAPI-Host": "toronto-pearson-airport.p.rapidapi.com",
    },
  };

  axios
    .request(options)
    .then(function (response) {
      console.log(response.data);
      res.json(response.data.slice(0, 5));
    })
    .catch(function (error) {
      console.error(error);
    });
});

app.listen(PORT, () => console.log("Running on PORT " + PORT));
